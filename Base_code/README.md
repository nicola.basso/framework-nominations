# Base code for every framework

> What is this?

Here you can find the UI used on every framework, but with no functionality at all.

> Why?

Because every framework follow a different philosophy, like some of those use MVC, or MVVM, or just VM.

> How?

On every framework you can find a guide on how to adapt this code to run properly, with data binds too.

## Project overview

A good cross-platform framework should provide some methods or plugins to run native code, like Camera, Filesystem, Sensors, Audio, ...  
So this UI create a wireframe for some of those.

### Basic libraries

Using bootstrap is a little to OP for this, so I opted for an inline css library: [Tachyons](https://github.com/tachyons-css/tachyons).

Also using NodeJS would be a problem for some frameworks, so this is just pure HTML+CSS+JS and Tachyons, nothing more, nothing less.

### Usage

You can try it here (TODO on gitlab wiki)

## License

This example is under MIT License

[LICENSE file](LICENSE)
