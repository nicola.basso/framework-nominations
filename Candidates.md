# Candidates

> Refer to "Frameworks scraping" epic on Jira

##### tl;dr

- Meteor
- Flutter
- Ionic
- Nativescript
- Electron
- Xamarin
- React Native
- Onsen.io
- Ktor
- Ruby on rails
- Felgo
- Phoenix

## Status List

> Updated: 23/09/2020

1. [Meteor](Frameworks/Meteor/)
    - [report README](Frameworks/Meteor/README.md)
    - [examples](Frameworks/Meteor/Meteor_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Flutter](Frameworks/Flutter)
    - [report README](Frameworks/Flutter/README.md)
    - [examples](Frameworks/Flutter/Flutter_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Ionic](Frameworks/Ionic)
    - [report README](Frameworks/Ionic/README.md)
    - [examples](Frameworks/Ionic/Ionic_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Nativescript](Frameworks/Nativescript)
    - [report README](Frameworks/Nativescript/README.md)
    - [examples](Frameworks/Nativescript/Nativescript_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Electron](Frameworks/Electron)
    - [report README](Frameworks/Electron/README.md)
    - [examples](Frameworks/Electron/Electron_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Xamarin](Frameworks/Xamarin)
    - [report README](Frameworks/Xamarin/README.md)
    - [examples](Frameworks/Xamarin/Xamarin_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [React Native](Frameworks/React_Native)
    - [report README](Frameworks/React_Native/README.md)
    - [examples](Frameworks/React_Native/React_Native_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Onsen.io](Frameworks/Onsen)
    - [report README](Frameworks/Onsen/README.md)
    - [examples](Frameworks/Onsen/Onsen_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Ktor](Frameworks/Ktor)
    - [report README](Frameworks/Ktor/README.md)
    - [examples](Frameworks/Ktor/Ktor_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Ruby on Rails](Frameworks/RubyOnRails)
    - [report README](Frameworks/RubyOnRails/README.md)
    - [examples](Frameworks/RubyOnRails/RubyOnRails_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Felgo](Frameworks/Felgo)
    - [report README](Frameworks/Felgo/README.md)
    - [examples](Frameworks/Felgo/Felgo_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report
1. [Phoenix](Frameworks/Phoenix)
    - [report README](Frameworks/Phoenix/README.md)
    - [examples](Frameworks/Phoenix/Phoenix_Examples/INSTALL.md)
    - Status:
        1. [x] Found data
        1. [ ] Basic report created
        1. [ ] Example created
        1. [ ] Final report

---

## Preliminary results

1. TODO
1. ...

## Finals

1. TODO
1. TODO
1. TODO

## Winner

### Motivation
