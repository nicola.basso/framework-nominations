# React Native

[React Native website](https://reactnative.dev/)

- Programming languages:
    - **Javascript**
    - NodeJS
- Used technologies:
    - **React**
- Platform development:
    - **Android**
    - **iOS**
    - **Web**
    - **Windows** (UWP)
- Initial release:
    - v0.1.0 released on date **26/03/2015**
- Stable release:
    - v.0.63.1 release on date **14/07/2020**
- Current state:
    - 23/09/2020: **active**
- Community level:
    - **4**
    - [React Native groups](https://reactnative.dev/help)
- Documentation level:
    - **3**
    - [React Native docs](https://reactnative.dev/docs/getting-started)
- Performance level:
    - TODO
- Common problems
    - Some iOS and Android bugs
    - [github issues](https://github.com/facebook/react-native/labels/Bug)
- Example codes:
    - [Open source React native apps list](https://github.com/ReactNativeNews/React-Native-Apps)
- License
    - MIT license
    - [MIT License github file](https://github.com/facebook/react-native/blob/master/LICENSE)

---

## Code reusability

## Customization


---

- ## Strong points
    1. TODO
- ## Weak points
    1. TODO
---

### Products using this framework

#### Personal considerations
