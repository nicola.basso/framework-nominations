# Some useful tools that enhance react native development

- [Live preview Expo.io](https://expo.io/)

# Some articles about react native

- [History of React native](https://medium.com/react-native-development/a-brief-history-of-react-native-aae11f4ca39)
- [Web app with React native](https://heartbeat.fritz.ai/how-to-build-a-web-app-with-react-native-b93575a16a5e)
- [Mobile wep app with React native](https://www.digitalocean.com/community/tutorials/build-mobile-friendly-web-apps-with-react-native-web)
